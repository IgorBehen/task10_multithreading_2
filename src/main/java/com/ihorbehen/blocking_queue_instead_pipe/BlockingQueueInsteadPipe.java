package com.ihorbehen.blocking_queue_instead_pipe;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;


public class BlockingQueueInsteadPipe {
    BlockingQueue<Integer> blockingQueue = new LinkedBlockingDeque<>(1);
    int [] arr =  {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
 
    public static void main(String[] args) {
        BlockingQueueInsteadPipe bqip = new BlockingQueueInsteadPipe();
        bqip.write().start();
        bqip.read().start();

    }
    private Thread write() {
        return new Thread(() -> {
            try {
                    for (int num : arr) {
                        blockingQueue.put(num);
                    }
                    blockingQueue.put(-1);
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        });
    }

    private Thread read() {
        return new Thread(() -> {
            int num;
            try {
                while ((num = blockingQueue.take()) != -1) {
                    System.out.print(num);
                    Thread.sleep(1000);
                }
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        });
    }
}

