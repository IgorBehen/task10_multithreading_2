package com.ihorbehen.read_write_lock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Create safe to use ArrayList<>()
 *
 * @param <E>
 */
public class OwnReadWriteLock<E> {
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final Lock readLock = readWriteLock.readLock();
    private final Lock writeLock = readWriteLock.writeLock();
    List<E> list = new ArrayList<>();

    public void set(E o) {
        int counter = 0;
        writeLock.lock();
        try {
            while (counter < 20) {
                counter++;
                Thread.sleep(500);
                System.out.println("Adding element by thread " + Thread.currentThread().getName());
                list.add(o);
            }
        } catch (InterruptedException ex) {
        } finally {
            writeLock.unlock();
        }
    }

    public E get(int i) {
        readLock.lock();
        try {
            System.out.println("Printing elements by thread" + Thread.currentThread().getName());
            return list.get(i);
        } finally {
            readLock.unlock();
        }
    }
}

class AddAmountTask implements Runnable {
    OwnReadWriteLock orwl;
    Object o = new Object();

    AddAmountTask(OwnReadWriteLock ncc) {
        orwl = ncc;
    }

    public void run() {
        orwl.set(o);
    }
}

class ExplicitLock {

    public static void main(String[] args) {
        OwnReadWriteLock<Integer> ownReadWriteLock = new OwnReadWriteLock<>();
        AddAmountTask t = new AddAmountTask(ownReadWriteLock);
        Thread t1 = new Thread(t);
        Thread t2 = new Thread(t);
        Thread t3 = new Thread(t);
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("End.");
    }
}
